var arrayInt = [1, 2, 3, 4]
var arrayString = ["1", "2", "3", "4"]

struct MyArray<T> {
    
    var `data` = [T]()
    
    init(_ input: [T]) {
        self.`data` = input
    }
    
}

class TryGeneric {

    func printArray() {
        print(MyArray(arrayInt).`data`)
        print(MyArray(arrayString).`data`)
    }
    
}

var tryGeneric = TryGeneric()
tryGeneric.printArray()
